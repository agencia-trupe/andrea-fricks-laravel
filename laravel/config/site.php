<?php

return [

    'name'        => 'Ândrea Fricks',
    'title'       => 'Ândrea Fricks',
    'description' => 'Design de interiores, residenciais e comerciais, com personalidade, beleza e atitude nos ambientes.',
    'keywords'    => 'decoração de interiores, design de interiores, arquiteta, arquitetura de ambientes',
    'share_image' => 'marca-andreafricks.jpg',
    'analytics'   => 'UA-55334455-1'

];
