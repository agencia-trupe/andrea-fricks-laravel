<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntesEDepoisTable extends Migration
{
    public function up()
    {
        Schema::create('antes_e_depois', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('antes');
            $table->string('depois');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('antes_e_depois');
    }
}
