<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ProjetosTableSeeder::class);
        $this->call(MidiaTableSeeder::class);
        $this->call(AntesEDepoisTableSeeder::class);

        Model::reguard();
    }
}
