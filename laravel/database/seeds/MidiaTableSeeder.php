<?php

use Illuminate\Database\Seeder;

class MidiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('midia')->insert([
            [
                'nome' => 'Revista O Globo',
                'slug' => 'revista-o-globo',
                'capa' => 'revista-o-globo.jpg',
            ],
            [
                'nome' => 'Casa Vogue',
                'slug' => 'casa-vogue',
                'capa' => 'casa-vogue.jpg',
            ],
            [
                'nome' => 'Bela Casa',
                'slug' => 'bela-casa',
                'capa' => 'bela-casa.jpg',
            ],
        ]);

        foreach (range(1, 3) as $i) {
            DB::table('midia_imagens')->insert([
                'midia_id' => '1',
                'imagem'   => "revista-o-globo-{$i}.jpg"
            ]);
        }

        foreach (range(1, 3) as $i) {
            DB::table('midia_imagens')->insert([
                'midia_id' => '2',
                'imagem'   => "casa-vogue-{$i}.jpg"
            ]);
        }

        foreach (range(1, 2) as $i) {
            DB::table('midia_imagens')->insert([
                'midia_id' => '3',
                'imagem'   => "bela-casa-{$i}.jpg"
            ]);
        }
    }
}
