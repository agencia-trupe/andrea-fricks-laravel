<?php

use Illuminate\Database\Seeder;

class AntesEDepoisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $registros = [];
        foreach (range(1, 23) as $i) {
            $registros[] = [
                'antes'  => "{$i}-antes.jpg",
                'depois' => "{$i}-depois.jpg",
            ];
        }

        DB::table('antes_e_depois')->insert(array_reverse($registros));
    }
}
