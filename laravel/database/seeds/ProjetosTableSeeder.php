<?php

use Illuminate\Database\Seeder;

class ProjetosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projetos = [
            [
                'nome' => 'Apartamento Moema',
                'slug' => 'apto-moema',
                'capa' => 'apto-moema.jpg'
            ],
            [
                'nome' => 'Apartamento Morumbi',
                'slug' => 'apto-morumbi',
                'capa' => 'apto-morumbi.jpg'
            ],
            [
                'nome' => 'Casa Brooklin',
                'slug' => 'casa-brooklin',
                'capa' => 'casa-brooklin.jpg'
            ],
            [
                'nome' => 'Esmalteca – Esmalteria',
                'slug' => 'esmalteca',
                'capa' => 'esmalteca.jpg'
            ],
            [
                'nome' => 'Apartamento Panamby',
                'slug' => 'panamby',
                'capa' => 'panamby.jpg'
            ],
            [
                'nome' => 'Vila Leopoldina',
                'slug' => 'leopoldina',
                'capa' => 'vila-leopoldina.jpg'
            ],
            [
                'nome' => 'Apartamento Centro',
                'slug' => 'centro',
                'capa' => 'centro.jpg'
            ],
            [
                'nome' => 'Alphaville',
                'slug' => 'alphaville',
                'capa' => 'alphaville.jpg'
            ],
            [
                'nome' => 'Apartamento Bela Vista',
                'slug' => 'bela-vista',
                'capa' => 'bela-vista.jpg'
            ],
            [
                'nome' => 'Apartamento Vila Mariana',
                'slug' => 'vila-mariana',
                'capa' => 'vila-mariana.jpg'
            ],
            [
                'nome' => 'Estúdio de Criação',
                'slug' => 'estudio',
                'capa' => 'estudio.jpg'
            ],
            [
                'nome' => 'Apartamento Tamboré',
                'slug' => 'tambore',
                'capa' => 'tambore.jpg'
            ],
            [
                'nome' => 'Apartamento Brooklin Novo',
                'slug' => 'brooklin',
                'capa' => 'brooklin.jpg'
            ],
            [
                'nome' => 'Hall Tatuapé',
                'slug' => 'hall-tatuape',
                'capa' => 'hall-tatuape.jpg'
            ],
            [
                'nome' => 'Apartamento Campo Belo',
                'slug' => 'ap-campo-belo',
                'capa' => 'ap-campo-belo.jpg'
            ],
            [
                'nome' => 'Apartamento Central Park',
                'slug' => 'ap-central-park-2',
                'capa' => 'ap-central-park-2.jpg'
            ],
            [
                'nome' => 'Apartamento Mooca',
                'slug' => 'ap-mooca',
                'capa' => 'ap-mooca.jpg'
            ],
            [
                'nome' => 'Apartamento Central Park',
                'slug' => 'ap-central-park',
                'capa' => 'ap-central-park.jpg'
            ],
            [
                'nome' => 'Sede Voran Tecnologia',
                'slug' => 'sede-voran-tecnologia',
                'capa' => 'sede-voran-tecnologia.jpg'
            ],
            [
                'nome' => 'Apartamento Barra RJ',
                'slug' => 'barra',
                'capa' => 'barra.jpg'
            ],
            [
                'nome' => 'Morar Mais RJ',
                'slug' => 'morar-mais-rj',
                'capa' => 'morar-mais-rj.jpg'
            ],
            [
                'nome' => 'Quartos Kadu e Nina Lagoa RJ',
                'slug' => 'kadu-nina',
                'capa' => 'kadu-nina.jpg'
            ]
        ];

        DB::table('projetos')->insert(array_reverse($projetos));

        /* INSERE IMAGENS DA PASTA PROJETOS A PARTIR DOS SLUGS
        foreach (App\Models\Projeto::get() as $projeto) {
            foreach (glob('assets/img/projetos/imagens/'.$projeto->slug.'*') as $img) {
                $projeto->imagens()->create([
                    'imagem' => basename($img)
                ]);
            }
        }
        */
    }
}
