@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id="box_listagem_projetos">
            <div class='espacamento'></div>
            <div class="clear"></div>

            @foreach($midia as $projeto)
                <a class="{{ $projeto->slug }}" href="{{ asset('assets/img/midia/imagens/'.$projeto->imagens->first()->imagem) }}">
                    <div class="box_fotos_projeto">
                        <div class="tint">
                        <img class="thumb_projeto" src="{{ asset('assets/img/midia/capa/'.$projeto->capa) }}" alt="">
                        </div>
                        <div class="nome_projeto">
                            {{ $projeto->nome }}
                            <div class='seta_amarela'>
                            <img src="{{ asset('assets/img/seta-amarela.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach

            @foreach($midia as $projeto)
                @foreach($projeto->imagens as $imagem)
                    @unless($projeto->imagens->first() === $imagem)
                    <a style="display:none" class="{{ $projeto->slug }}" href="{{ asset('assets/img/midia/imagens/'.$imagem->imagem) }}"></a>
                    @endunless
                @endforeach
            @endforeach

          <div class="clear"></div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
    $(document).ready(function() {
@foreach($midia as $projeto)
        $(".{{ $projeto->slug }}").colorbox({ rel: "{{ $projeto->slug }}" });
@endforeach

        $('.tint').mouseover(function(){
            $(this).addClass('thumb_hover');
            $(this).removeClass('thumb_sem_hover');
        });
        $('.tint').mouseout(function(){
            $(this).addClass('thumb_sem_hover');
            $(this).removeClass('thumb_hover');
        });
    });
    </script>

@endsection