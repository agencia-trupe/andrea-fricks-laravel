@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id='texto_servico'>
            <div id="box_form" class="projetos-especiais">
                <h1>PROJETOS ESPECIAIS</h1>
                <p>
                    Temos uma condição especial para os proprietários dos empreendimentos abaixo.<br>
                    Estudamos as plantas e desenvolveremos projetos de acordo com sua necessidade e perfil.<br>
                    Além do valor diferenciado, temos serviços exclusivos agregados ao projeto e ao gerenciamento da obra.
                </p>
                <p>Selecione seu empreendimento e preencha os campos solicitados.</p>

                <form method="POST" action="{{ route('projetos-especiais.envio') }}">
                    {{ csrf_field() }}

                    <div class="empreendimentos">
                        @foreach($empreendimentos as $empreendimento)
                        <label class="empreendimento">
                            <input type="radio" name="empreendimento" value="{{ $empreendimento->nome }}" @if($empreendimento === $empreendimentos->first() && old('empreendimento') == null) checked @elseif(old('empreendimento') === $empreendimento->nome) checked @endif>
                            <img src="{{ asset('assets/img/empreendimentos/'.$empreendimento->imagem) }}" alt="">
                        </label>
                        @endforeach
                    </div>

                    <label for="unidade">
                        unidade/apartamento
                        <input name="unidade" id="unidade" type="text" value="{{ old('unidade') }}" required>
                    </label>
                    <label for="nome">
                        nome
                        <input name="nome" id="nome" type="text" value="{{ old('nome') }}" required>
                    </label>
                    <label for="email">
                        email
                        <input name="email" id="email" type="email" value="{{ old('email') }}" required>
                    </label>

                    @if(session('envio'))
                        <div class="status sucesso">
                            Mensagem enviada com sucesso!
                        </div>
                    @elseif((count($errors) > 0))
                        <div class="status erro">
                            Preencha os campos corretamente.
                        </div>
                    @elseif(session('erro'))
                        <div class="status erro">
                            Ocorreu um erro. Tente novamente.
                        </div>
                    @endif

                    <p>Em breve você receberá nossa proposta com valor especial. Aguarde!</p>

                    <input type="submit" id="enviar" value="[ ENVIAR ]">
                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>

@endsection
