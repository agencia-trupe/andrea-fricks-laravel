@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id="box_antes_depois">
            <div id="box_antes">
                <div id="box_texto_antes">Antes</div>
                <div id="box_foto_antes"><img class='changeable-antes' src="{{ asset('assets/img/antes-e-depois/'.$fotos->first()->antes) }}" alt=""></div>
                <div id="botao_proximo" class='button-changeable' data-op='+'>próximo <img src="{{ asset('assets/img/seta-prox.png') }}" alt=""></div>
            </div>

            <div id="box_depois">
                <div id="box_texto_depois">Depois</div>
                <div id="box_foto_depois"><img class='changeable-depois' src="{{ asset('assets/img/antes-e-depois/'.$fotos->first()->depois) }}" alt=""></div>
                <div id="botao_anterior" class='button-changeable' data-op='-'><img src=" {{ asset('assets/img/seta-ant.png') }}" alt=""> anterior</div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <script>
        var fotos = [], fotosTotal = {{ count($fotos) }}, fotoIndex = 0;
@foreach($fotos as $foto)
        fotos.push(['{{ asset('assets/img/antes-e-depois/'.$foto->antes) }}', '{{ asset('assets/img/antes-e-depois/'.$foto->depois) }}']);
@endforeach
    </script>

@endsection

@section('scripts')

    <script>
    $(document).ready(function() {
        function change_picture(c, op){
            if (op == '+') {
                if (c == fotosTotal - 1) c = 0;
                else c++;
            }
            if (op == '-') {
                if (c == 0) c = fotosTotal -1;
                else c--;
            }

            $('.changeable-antes').attr('src', fotos[c][0]).hide().fadeIn('slow');
            $('.changeable-depois').attr('src', fotos[c][1]).hide().fadeIn('slow');

            fotoIndex = c;
        }

        $('.button-changeable').bind('click', function(){
            change_picture(fotoIndex, $(this).attr('data-op'))
        });
    });
    </script>

@endsection
