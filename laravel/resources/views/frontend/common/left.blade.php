<div id='container_logo'>
    <div id='foto_logo'>
        <img class='logo_redirect' data-redirect="{{ route('home') }}" src="{{ asset('assets/img/logo-andrea-fricks.png') }}">
    </div>
</div>

<div id='container_navegacao'>
    <div id='nav'>
        <div @if(Route::currentRouteName() === 'perfil') class="menu_selected" @endif>
            <a data-redirect="{{ route('perfil') }}">Perfil</a>
        </div>
        <div @if(Route::currentRouteName() === 'midia') class="menu_selected" @endif>
            <a data-redirect="{{ route('midia') }}">Mídia</a>
        </div>
        <div @if(str_is('projetos*', Route::currentRouteName()) && Route::currentRouteName() !== 'projetos-especiais') class="menu_selected" @endif>
            <a data-redirect="{{ route('projetos') }}">Projetos</a>
        </div>
        <div @if(Route::currentRouteName() === 'antes-e-depois') class="menu_selected" @endif>
            <a data-redirect="{{ route('antes-e-depois') }}">Antes e Depois</a>
        </div>
        <div @if(Route::currentRouteName() === 'projetos-especiais') class="menu_selected" @endif>
            <a data-redirect="{{ route('projetos-especiais') }}">Projetos Especiais</a>
        </div>
        <div @if(Route::currentRouteName() === 'contato') class="menu_selected" @endif>
            <a data-redirect="{{ route('contato') }}">Contato</a>
        </div>
    </div>
</div>

<div id='container_footer'>
    <ul id='footer_list'>
        <a href="https://www.facebook.com/pages/%C3%82ndrea-Fricks-Design-de-interiores/224295167600859" target="_blank" class="facebook">facebook</a>
        <li>11 3266 2808 </li>
        <li>&nbsp;</li>
        <li>Rua Tupi 496 cj 5 – Santa Cecília</li>
        <li>01233-001 – São Paulo, SP</li>
        <li>&nbsp;</li>
        <li><a href="mailto:contato@afinteriores.com.br">contato@afinteriores.com.br</a></li>
    </ul>

    <a href="http://www.andreafricksinteriores.com.br/r">
        <img src="{{ asset('assets/img/busca-cliente.png') }}" alt="Busca Cliente" title="Busca Cliente">
    </a>
</div>
