<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ config('site.description') }}">
    <meta name="keywords" content="{{ config('site.keywords') }}">

    <meta property="og:title" content="@if(isset($seoTitle)) {{ $seoTitle }} - @endif{{ config('site.title') }}">
    <meta property="og:description" content="{{ config('site.description') }}">
    <meta property="og:site_name" content="@if(isset($seoTitle)) {{ $seoTitle }} - @endif{{ config('site.title') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{ asset('assets/img/'.config('site.share_image')) }}">

    <title>@if(isset($seoTitle)) {{ $seoTitle }} - @endif{{ config('site.title') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/js/jquery-ui-1.10.3.custom.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/js/colorbox-master/example2/colorbox.css') }}">
</head>
<body>
    <div class="esconde"></div>
    <div id="outer">
        <div id='container_esquerda'>
            @include('frontend.common.left')
        </div>
        <div id="container_direita" @if(Route::currentRouteName() === 'home') style="background:#fff" @endif>
            @yield('content')
        </div>
    </div>

    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script src="{{ asset('assets/js/colorbox-master/jquery.colorbox.js') }}"></script>
    <script src="{{ asset('assets/js/greyscale.js') }}"></script>
    <script src="{{ asset('assets/js/cycle.js') }}"></script>
    <script>
    function ajeita_rodape(){
        $('#container_footer').css('top', $(window).height() - 120 + $(window).scrollTop()).show();
    }
    ajeita_rodape();

    $('document').ready(function(){
        $('#container_esquerda').height($('#container_direita').height() + 1).show();

        $(window).resize(function() {
            $('#container_esquerda').height($('#container_direita').height() + 1);
            ajeita_rodape();
        });
        $(window).scroll(function() {
            $('#container_esquerda').height($('#container_direita').height() + 1);
            ajeita_rodape();
        });

        setTimeout(function(){
            $('#container_direita').addClass('active').show().css({
                left: -( $('#container_direita').width() - 288)
            }).animate({
                left: 288
            }, 900, 'easeInOutCubic');
        }, 50);


        $('#nav div a, .logo_redirect, .redirect').click(function () {
            el = $(this);
            $('#container_direita').animate({
                left:  -$('#container_direita').width() + 288
            }, 900, 'easeInOutCubic', function(){
                location.href = el.attr('data-redirect');
            });
        });
    });
    </script>
@yield('scripts')

@if(config('site.analytics'))
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ config("site.analytics") }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
