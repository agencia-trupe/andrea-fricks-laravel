@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id="box_listagem_projetos">
            <div class="espacamento"></div>
            <div class="clear"></div>

            <div id='foto_principal_container'>
                @foreach($projeto->imagens as $imagem)
                    <img @if($projeto->imagens->first() !== $imagem) style="display:none;" @endif
                        src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}"
                        data-thumb="{{ asset('assets/img/projetos/imagens/thumbs-small/'.$imagem->imagem) }}">
                @endforeach
            </div>

            <a href="#" title="Imagem Anterior" class="button-changeable" id="seta_esquerda"><img class='seta_box esq' src="{{ asset('assets/img/seta-amarela-esq.png') }}" alt="Imagem Anterior"></a>
            <a href="#" title="Próxima Imagem" class="button-changeable" id="seta_direita"><img class='seta_box' src="{{ asset('assets/img/seta-amarela.png') }}" alt="Próxima Imagem"></a>

            <div class="clear"></div>
            <div class="titulo_projeto">{{ $projeto->nome }}</div>
            <div class="clear"></div>

            <div class="listagem_fotos" id="img-nav"></div>

            <div class="clear"></div>
            <div id='botao_voltar'>
                <a class="none redirect" data-redirect="{{ route('projetos') }}"><img src="{{ asset('assets/img/seta-anterior.png') }}" alt=""> voltar </div><a>
            </div>
        </div>

@endsection

@section('scripts')

    <script>
    $('document').ready( function(){
        $('#foto_principal_container').cycle({
            timeout : 0,
            next : $('#seta_direita'),
            prev : $('#seta_esquerda'),
            pager : $('#img-nav'),
            pagerAnchorBuilder: function(idx, slide) {
                return "<a href='" + slide.src + "' class='foto_projeto' title='Ver imagem ampliada' target='_blank'><img class='foto_projeto_img greyScale' src='" + slide.getAttribute('data-thumb') + "'></a>";
            }
        });
    });
    </script>

@endsection
