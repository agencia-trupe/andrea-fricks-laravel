@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id="box_listagem_projetos">
            <div class='espacamento'></div>
            <div class="clear"></div>
            @foreach($projetos as $projeto)
                <a class="redirect" data-redirect="{{ route('projetos.show', $projeto->slug) }}">
                    <div class="box_fotos_projeto">
                    <div class="tint">
                        <img class="thumb_projeto redirect" src="{{ asset('assets/img/projetos/capa/'.$projeto->capa) }}" alt="">
                        </div>
                        <div class="nome_projeto">
                            {{ $projeto->nome }}
                            <div class='seta_amarela'>
                                <img src="{{ asset('assets/img/seta-amarela.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
          <div class="clear"></div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
    $(document).ready(function() {
        $('.tint').mouseover(function(){
            $(this).addClass('thumb_hover');
            $(this).removeClass('thumb_sem_hover');
        });
        $('.tint').mouseout(function(){
            $(this).addClass('thumb_sem_hover');
            $(this).removeClass('thumb_hover');
        });
    });
    </script>

@endsection