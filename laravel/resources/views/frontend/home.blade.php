@extends('frontend.common.template')

@section('content')

    <div id="container_fotos" class='container-fotos-home'>
        @for($i = 0; $i < 30; $i++)
            <div class="quadrado_wrapper">
                @for($j = 0; $j < 4; $j++)
                    @if($shuffle[$j][$i] === 'quadrado')
                    <div class="quadrado quadrado_amarelo" @if($j > 0) style="display:none" @endif></div>
                    @else
                    <a href="{{ route('projetos.show', $shuffle[$j][$i]->projeto->slug) }}" class='quadrado quadrado_foto' @if($j > 0) style="display:none" @endif>
                        <img class="foto_home_1" class="img_quadrado" src="{{ asset('assets/img/projetos/imagens/thumbs/'.$shuffle[$j][$i]->imagem) }}" alt="">
                    </a>
                    @endif
                @endfor
            </div>
        @endfor
        <div class="clear"></div>
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            var $quadrados = $('.quadrado_wrapper');
            $quadrados.cycle({ timeout: 0 });

            function muda() {
                $quadrados.each(function(i) {
                    var _this = $(this);
                    setTimeout(function() {
                        _this.cycle('next');
                    }, (i * 350));
                });

                setTimeout(muda, 10150);
            }

            setTimeout(muda, 3000);
        });
    </script>

@endsection
