@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id='texto_servico'>
            <div id="box_form">
                <form method="POST" action="{{ route('contato.envio') }}">
                    {{ csrf_field() }}
                    <label for="nome">
                        nome
                        <input name="nome" id="nome" type="text" value="{{ old('nome') }}" required>
                    </label>
                    <label for="email">
                        email
                        <input name="email" id="email" type="email" value="{{ old('email') }}" required>
                    </label>
                    <label for="telefone">
                        telefone
                        <input name="telefone" id="telefone" type="text" value="{{ old('telefone') }}">
                    </label>
                    <label for="mensagem">
                        mensagem
                        <textarea name="mensagem" id="mensagem" required>{{ old('mensagem') }}</textarea>
                    </label>

                    @if(session('envio'))
                        <div class="status sucesso">
                            Mensagem enviada com sucesso!
                        </div>
                    @elseif((count($errors) > 0))
                        <div class="status erro">
                            Preencha os campos corretamente.
                        </div>
                    @elseif(session('erro'))
                        <div class="status erro">
                            Ocorreu um erro. Tente novamente.
                        </div>
                    @endif

                    <input type="submit" id="enviar" value="[ ENVIAR ]">
                </form>
                <div id="contato_contato">
                    11 3266·2808 <br>
                    <a href="mailto:contato@afinteriores.com.br">
                        contato@afinteriores.com.br
                    </a>
                </div>
            </div>
        </div>
    </div>

@if(session('envio'))
<!-- Google Code for contato Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 972572024;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "MLIpCKCyyVYQ-IrhzwM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/972572024/?label=MLIpCKCyyVYQ-IrhzwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
@endif

@endsection
