@extends('frontend.common.template')

@section('content')

    <div id="container_fotos">
        <div id='texto_perfil'>
            <div id='caixa_texto_perfil'>
                <span class='titulo1'>"Novas solu&ccedil;&otilde;es</span><br/>
                <span class='titulo2'>para velhos problemas"</span>
                <br/>
                <br/>
                <br/>
                <p class='perfil_descricao'>Pós-graduada em Design de Interiores pelo SENAC-SP, a trajetória dessa carioca residente em São Paulo é marcada por desafios.</p>
                <p class='perfil_descricao'>Inspiração e funcionalidade são suas ferramentas de criação, sua prioridade é atender expectativas, unindo beleza e atitude em cada ambiente. Projetos residenciais e comerciais com a personalidade do dono.</p>
                <p class='perfil_descricao'>Ândrea considera fundamental o compromisso com prazos e busca a excelência de seu trabalho desde as etapas iniciais.</p>
                <p class='perfil_descricao'>Há nela uma constante preocupação em se modernizar, reciclar seus conhecimentos e conhecer novas possibilidades.</p>
                <p class='perfil_descricao'></p>
            </div>
        </div>

        <div id='foto_perfil'>
            <img src="{{ asset('assets/img/foto-andrea.jpg') }}">
        </div>
    </div>

@endsection
