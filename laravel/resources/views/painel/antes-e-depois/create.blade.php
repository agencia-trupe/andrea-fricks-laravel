@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Antes e Depois /</small> Adicionar Antes e Depois</h2>
    </legend>

    {!! Form::open(['route' => 'painel.antes-e-depois.store', 'files' => true]) !!}

        @include('painel.antes-e-depois.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
