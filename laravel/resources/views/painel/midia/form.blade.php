@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/midia/capa/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 230px;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.midia.index') }}" class="btn btn-default btn-voltar">Voltar</a>
