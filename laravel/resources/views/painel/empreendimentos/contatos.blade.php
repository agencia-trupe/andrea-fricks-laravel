@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.empreendimentos.index') }}" title="Voltar para Empreendimentos" class="btn btn-sm btn-default">
        &larr; Voltar para Empreendimentos
    </a>

    <legend>
        <h2><small>Empreendimentos /</small> Contatos Recebidos</h2>
    </legend>

    @if(!count($contatos))
    <div class="alert alert-warning" role="alert">Nenhuma mensagem recebida.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Empreendimento</th>
                <th>Unidade</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($contatos as $contato)

            <tr class="tr-row">
                <td>{{ $contato->created_at }}</td>
                <td>{{ $contato->empreendimento }}</td>
                <td>{{ $contato->unidade }}</td>
                <td>{{ $contato->nome }}</td>
                <td><a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.empreendimentos.contatos.destroy', $contato->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $contatos->render() !!}
    @endif

@endsection
