@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Empreendimentos /</small> Editar Empreendimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.empreendimentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.empreendimentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
