@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Empreendimentos /</small> Adicionar Empreendimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.empreendimentos.store', 'files' => true]) !!}

        @include('painel.empreendimentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
