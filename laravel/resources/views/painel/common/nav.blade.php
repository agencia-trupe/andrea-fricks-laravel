<ul class="nav navbar-nav">
    <li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.projetos.index') }}">Projetos</a>
    </li>
    <li @if(str_is('painel.midia*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.midia.index') }}">Mídia</a>
    </li>
    <li @if(str_is('painel.antes-e-depois*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.antes-e-depois.index') }}">Antes e Depois</a>
    </li>
	<li @if(str_is('painel.empreendimentos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.empreendimentos.index') }}">Empreendimentos</a>
	</li>
</ul>
