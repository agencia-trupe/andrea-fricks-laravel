<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MidiaImagem extends Model
{
    protected $table = 'midia_imagens';

    protected $guarded = ['id'];

    public function scopeMidia($query, $id)
    {
        return $query->where('midia_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
