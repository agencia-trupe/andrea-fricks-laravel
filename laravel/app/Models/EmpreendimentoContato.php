<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpreendimentoContato extends Model
{
    protected $table = 'empreendimentos_contatos';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
}
