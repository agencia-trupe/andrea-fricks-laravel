<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AntesEDepois extends Model
{
    protected $table = 'antes_e_depois';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
