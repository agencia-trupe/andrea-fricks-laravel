<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('midia', 'MidiaController@index')->name('midia');
    Route::get('projetos', 'ProjetosController@index')->name('projetos');
    Route::get('projetos/{projeto_slug}', 'ProjetosController@show')->name('projetos.show');
    Route::get('antes-e-depois', 'AntesEDepoisController@index')->name('antes-e-depois');
    Route::get('projetos-especiais', 'ProjetosEspeciaisController@index')->name('projetos-especiais');
    Route::post('projetos-especiais', 'ProjetosEspeciaisController@envio')->name('projetos-especiais.envio');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@envio')->name('contato.envio');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('empreendimentos/contatos', 'EmpreendimentosController@contatos')->name('painel.empreendimentos.contatos');
        Route::delete('empreendimentos/contatos/{id}', 'EmpreendimentosController@contatosDestroy')->name('painel.empreendimentos.contatos.destroy');
		Route::resource('empreendimentos', 'EmpreendimentosController');
		Route::resource('midia', 'MidiaController');
        Route::get('midia/{midia}/imagens/clear', [
            'as'   => 'painel.midia.imagens.clear',
            'uses' => 'MidiaImagensController@clear'
        ]);
        Route::resource('midia.imagens', 'MidiaImagensController');
        Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');
		Route::resource('antes-e-depois', 'AntesEDepoisController');

        //Route::resource('contato/recebidos', 'ContatosRecebidosController');
        //Route::resource('contato', 'ContatoController');

        Route::resource('usuarios', 'UsuariosController');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
