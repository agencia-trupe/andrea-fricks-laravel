<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\AntesEDepois;

class AntesEDepoisController extends Controller
{
    public function index() {
        $fotos = AntesEDepois::ordenados()->get();

        view()->share('seoTitle', 'Antes e Depois');
        return view('frontend.antes-e-depois', compact('fotos'));
    }
}
