<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Empreendimento;
use App\Models\EmpreendimentoContato;

class ProjetosEspeciaisController extends Controller
{
    public function index() {
        $empreendimentos = Empreendimento::ordenados()->get();
        view()->share('seoTitle', 'Projetos Especiais');

        return view('frontend.projetos-especiais', compact('empreendimentos'));
    }

    public function envio(Request $request)
    {
        $this->validate($request, [
            'empreendimento' => 'required',
            'unidade'        => 'required',
            'nome'           => 'required',
            'email'          => 'required|email'
        ]);

        try {

            EmpreendimentoContato::create($request->all());

            \Mail::send('emails.projetos-especiais', $request->all(), function($message) use ($request)
            {
                $message->to('contato@afinteriores.com.br', config('site.name'))
                    ->subject('['.config('site.name').'] Projetos Especiais')
                    ->replyTo($request->get('email'), $request->get('nome'));
            });

        } catch (\Exception $e) {

            $request->session()->flash('erro', true);
            return back()->withInput();

        }

        return back()->with('envio', true);
    }
}
