<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjetoImagem;

class HomeController extends Controller
{
    public function index()
    {
        $imagens = ProjetoImagem::orderByRaw("RAND()")->limit(50)->get();
        foreach(range(1, 8) as $i) {
            $imagens->add('quadrado');
        }

        $shuffle = [];
        foreach(range(1, 4) as $j) {
            $shuffle[] = $imagens->shuffle();
        }

        return view('frontend.home', compact('shuffle'));
    }
}
