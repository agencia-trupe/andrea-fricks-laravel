<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index() {
        $projetos = Projeto::ordenados()->get();

        view()->share('seoTitle', 'Projetos');
        return view('frontend.projetos.index', compact('projetos'));
    }

    public function show(Projeto $projeto)
    {
        view()->share('seoTitle', 'Projeto: '.$projeto->nome);
        return view('frontend.projetos.show', compact('projeto', 'background'));
    }
}
