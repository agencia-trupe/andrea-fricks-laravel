<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContatoController extends Controller
{
    public function index() {
        view()->share('seoTitle', 'Contato');

        return view('frontend.contato');
    }

    public function envio(Request $request)
       {
           $this->validate($request, [
               'nome'     => 'required',
               'email'    => 'required|email',
               'mensagem' => 'required'
           ]);

           try {

               \Mail::send('emails.contato', $request->all(), function($message) use ($request)
               {
                   $message->to('contato@afinteriores.com.br', config('site.name'))
                           ->subject('['.config('site.name').'] Contato')
                           ->replyTo($request->get('email'), $request->get('nome'));
               });

           } catch (\Exception $e) {

                $request->session()->flash('erro', true);
                return back()->withInput();

           }

           return back()->with('envio', true);
       }
}
