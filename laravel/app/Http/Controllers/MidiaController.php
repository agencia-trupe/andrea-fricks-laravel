<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Midia;

class MidiaController extends Controller
{
    public function index() {
        $midia = Midia::with('imagens')->ordenados()->get();

        view()->share('seoTitle', 'Mídia');
        return view('frontend.midia', compact('midia'));
    }
}
