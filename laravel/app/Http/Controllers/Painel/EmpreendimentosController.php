<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpreendimentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Empreendimento;
use App\Models\EmpreendimentoContato;
use App\Helpers\CropImage;

class EmpreendimentosController extends Controller
{
    private $image_config = [
        'width'  => 150,
        'height' => 100,
        'path'   => 'assets/img/empreendimentos/'
    ];

    public function index()
    {
        $registros = Empreendimento::ordenados()->get();

        return view('painel.empreendimentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.empreendimentos.create');
    }

    public function store(EmpreendimentosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Empreendimento::create($input);
            return redirect()->route('painel.empreendimentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Empreendimento $registro)
    {
        return view('painel.empreendimentos.edit', compact('registro'));
    }

    public function update(EmpreendimentosRequest $request, Empreendimento $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.empreendimentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Empreendimento $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.empreendimentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function contatos()
    {
        $contatos = EmpreendimentoContato::orderBy('id', 'DESC')->paginate(15);

        return view('painel.empreendimentos.contatos', compact('contatos'));
    }

    public function contatosDestroy($id)
    {
        try {

            EmpreendimentoContato::findOrFail($id)->delete();
            return redirect()->route('painel.empreendimentos.contatos')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
