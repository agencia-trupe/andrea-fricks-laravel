<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Helpers\CropImage;

class ProjetosController extends Controller
{
    private $image_config = [
        'width'  => 230,
        'height' => 208,
        'path'   => 'assets/img/projetos/capa/'
    ];

    public function index()
    {
        $registros = Projeto::ordenados()->get();

        return view('painel.projetos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.projetos.create');
    }

    public function store(ProjetosRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = CropImage::make('capa', $this->image_config);

            Projeto::create($input);
            return redirect()->route('painel.projetos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $registro)
    {
        return view('painel.projetos.edit', compact('registro'));
    }

    public function update(ProjetosRequest $request, Projeto $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['capa'])) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.projetos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.projetos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
