<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MidiaRequest;
use App\Http\Controllers\Controller;

use App\Models\Midia;
use App\Helpers\CropImage;

class MidiaController extends Controller
{
    private $image_config = [
        'width'  => 230,
        'height' => 208,
        'path'   => 'assets/img/midia/capa/'
    ];

    public function index()
    {
        $registros = Midia::ordenados()->get();

        return view('painel.midia.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.midia.create');
    }

    public function store(MidiaRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = CropImage::make('capa', $this->image_config);

            Midia::create($input);
            return redirect()->route('painel.midia.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Midia $registro)
    {
        return view('painel.midia.edit', compact('registro'));
    }

    public function update(MidiaRequest $request, Midia $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['capa'])) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.midia.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Midia $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.midia.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
