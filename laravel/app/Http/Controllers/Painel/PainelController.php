<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class PainelController extends Controller
{

    public function index()
    {
        return view('painel.home');
    }

    public function order(Request $request)
    {
        if (!$request->ajax()) return false;

        $data  = $request->input('data');
        $table = $request->input('table');

        for ($i = 0; $i < count($data); $i++) {
            DB::table($table)->where('id', $data[$i])->update(array('ordem' => $i));
        }

        return json_encode($data);
    }

}
