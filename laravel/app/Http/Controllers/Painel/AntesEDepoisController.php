<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AntesEDepoisRequest;
use App\Http\Controllers\Controller;

use App\Models\AntesEDepois;
use App\Helpers\CropImage;

class AntesEDepoisController extends Controller
{
    private $image_config = [
        'width'  => 440,
        'height' => 330,
        'bg'     => '#000',
        'path'   => 'assets/img/antes-e-depois/'
    ];

    public function index()
    {
        $registros = AntesEDepois::ordenados()->get();

        return view('painel.antes-e-depois.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.antes-e-depois.create');
    }

    public function store(AntesEDepoisRequest $request)
    {
        try {

            $input = $request->all();
            $input['antes'] = CropImage::make('antes', $this->image_config);
            $input['depois'] = CropImage::make('depois', $this->image_config);

            AntesEDepois::create($input);
            return redirect()->route('painel.antes-e-depois.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AntesEDepois $registro)
    {
        return view('painel.antes-e-depois.edit', compact('registro'));
    }

    public function update(AntesEDepoisRequest $request, AntesEDepois $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['antes'])) {
                $input['antes'] = CropImage::make('antes', $this->image_config);
            }
            if (isset($input['depois'])) {
                $input['depois'] = CropImage::make('depois', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.antes-e-depois.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AntesEDepois $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.antes-e-depois.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
