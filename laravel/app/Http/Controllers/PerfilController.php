<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PerfilController extends Controller
{
    public function index() {
        view()->share('seoTitle', 'Perfil');

        return view('frontend.perfil');
    }
}
