<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AntesEDepoisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'antes' => 'required|image',
            'depois' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules = [
                'antes' => 'image',
                'depois' => 'image',
            ];
        }

        return $rules;
    }
}
