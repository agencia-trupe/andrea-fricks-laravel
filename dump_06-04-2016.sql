-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: andreafricks
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `antes_e_depois`
--

DROP TABLE IF EXISTS `antes_e_depois`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `antes_e_depois` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `antes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `depois` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `antes_e_depois`
--

LOCK TABLES `antes_e_depois` WRITE;
/*!40000 ALTER TABLE `antes_e_depois` DISABLE KEYS */;
INSERT INTO `antes_e_depois` VALUES (1,0,'23-antes.jpg','23-depois.jpg',NULL,NULL),(2,0,'22-antes.jpg','22-depois.jpg',NULL,NULL),(3,0,'21-antes.jpg','21-depois.jpg',NULL,NULL),(4,0,'20-antes.jpg','20-depois.jpg',NULL,NULL),(5,0,'19-antes.jpg','19-depois.jpg',NULL,NULL),(6,0,'18-antes.jpg','18-depois.jpg',NULL,NULL),(7,0,'17-antes.jpg','17-depois.jpg',NULL,NULL),(8,0,'16-antes.jpg','16-depois.jpg',NULL,NULL),(9,0,'15-antes.jpg','15-depois.jpg',NULL,NULL),(10,0,'14-antes.jpg','14-depois.jpg',NULL,NULL),(11,0,'13-antes.jpg','13-depois.jpg',NULL,NULL),(12,0,'12-antes.jpg','12-depois.jpg',NULL,NULL),(13,0,'11-antes.jpg','11-depois.jpg',NULL,NULL),(14,0,'10-antes.jpg','10-depois.jpg',NULL,NULL),(15,0,'9-antes.jpg','9-depois.jpg',NULL,NULL),(16,0,'8-antes.jpg','8-depois.jpg',NULL,NULL),(17,0,'7-antes.jpg','7-depois.jpg',NULL,NULL),(18,0,'6-antes.jpg','6-depois.jpg',NULL,NULL),(19,0,'5-antes.jpg','5-depois.jpg',NULL,NULL),(20,0,'4-antes.jpg','4-depois.jpg',NULL,NULL),(21,0,'3-antes.jpg','3-depois.jpg',NULL,NULL),(22,0,'2-antes.jpg','2-depois.jpg',NULL,NULL),(23,0,'1-antes.jpg','1-depois.jpg',NULL,NULL);
/*!40000 ALTER TABLE `antes_e_depois` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia`
--

DROP TABLE IF EXISTS `midia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia`
--

LOCK TABLES `midia` WRITE;
/*!40000 ALTER TABLE `midia` DISABLE KEYS */;
INSERT INTO `midia` VALUES (1,0,'Revista O Globo','revista-o-globo','revista-o-globo.jpg',NULL,NULL),(2,0,'Casa Vogue','casa-vogue','casa-vogue.jpg',NULL,NULL),(3,0,'Bela Casa','bela-casa','bela-casa.jpg',NULL,NULL);
/*!40000 ALTER TABLE `midia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia_imagens`
--

DROP TABLE IF EXISTS `midia_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `midia_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `midia_imagens_midia_id_foreign` (`midia_id`),
  CONSTRAINT `midia_imagens_midia_id_foreign` FOREIGN KEY (`midia_id`) REFERENCES `midia` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia_imagens`
--

LOCK TABLES `midia_imagens` WRITE;
/*!40000 ALTER TABLE `midia_imagens` DISABLE KEYS */;
INSERT INTO `midia_imagens` VALUES (1,1,0,'revista-o-globo-1.jpg',NULL,NULL),(2,1,0,'revista-o-globo-2.jpg',NULL,NULL),(3,1,0,'revista-o-globo-3.jpg',NULL,NULL),(4,2,0,'casa-vogue-1.jpg',NULL,NULL),(5,2,0,'casa-vogue-2.jpg',NULL,NULL),(6,2,0,'casa-vogue-3.jpg',NULL,NULL),(7,3,0,'bela-casa-1.jpg',NULL,NULL),(8,3,0,'bela-casa-2.jpg',NULL,NULL);
/*!40000 ALTER TABLE `midia_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_04_05_221824_create_antes_e_depois_table',1),('2016_04_05_224301_create_projetos_table',1),('2016_04_05_225145_create_midia_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,0,'Quartos Kadu e Nina Lagoa RJ em parceria com Marcia Martinez','kadu-nina','kadu-nina.jpg',NULL,NULL),(2,0,'Morar Mais RJ em parceria com Marcia Martinez','morar-mais-rj','morar-mais-rj.jpg',NULL,NULL),(3,0,'Apartamento Barra RJ em parceria com Marcia Martinez','barra','barra.jpg',NULL,NULL),(4,0,'Sede Voran Tecnologia','sede-voran-tecnologia','sede-voran-tecnologia.jpg',NULL,NULL),(5,0,'Apartamento Central Park','ap-central-park','ap-central-park.jpg',NULL,NULL),(6,0,'Apartamento Mooca','ap-mooca','ap-mooca.jpg',NULL,NULL),(7,0,'Apartamento Central Park','ap-central-park-2','ap-central-park-2.jpg',NULL,NULL),(8,0,'Apartamento Campo Belo','ap-campo-belo','ap-campo-belo.jpg',NULL,NULL),(9,0,'Hall Tatuapé','hall-tatuape','hall-tatuape.jpg',NULL,NULL),(10,0,'Apartamento Brooklin Novo','brooklin','brooklin.jpg',NULL,NULL),(11,0,'Apartamento Tamboré','tambore','tambore.jpg',NULL,NULL),(12,0,'Estúdio de Criação','estudio','estudio.jpg',NULL,NULL),(13,0,'Apartamento Vila Mariana','vila-mariana','vila-mariana.jpg',NULL,NULL),(14,0,'Apartamento Bela Vista','bela-vista','bela-vista.jpg',NULL,NULL),(15,0,'Alphaville','alphaville','alphaville.jpg',NULL,NULL),(16,0,'Apartamento Centro','centro','centro.jpg',NULL,NULL),(17,0,'Vila Leopoldina','leopoldina','vila-leopoldina.jpg',NULL,NULL),(18,0,'Apartamento Panamby','panamby','panamby.jpg',NULL,NULL),(19,0,'Esmalteca – Esmalteria','esmalteca','esmalteca.jpg',NULL,NULL),(20,0,'Casa Brooklin','casa-brooklin','casa-brooklin.jpg',NULL,NULL),(21,0,'Apartamento Morumbi','apto-morumbi','apto-morumbi.jpg',NULL,NULL),(22,0,'Apartamento Moema','apto-moema','apto-moema.jpg',NULL,NULL);
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (1,1,0,'kadu-nina-01.jpg','2016-04-06 19:25:40','2016-04-06 19:25:40'),(2,1,0,'kadu-nina-02.jpg','2016-04-06 19:25:40','2016-04-06 19:25:40'),(3,1,0,'kadu-nina-03.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(4,1,0,'kadu-nina-04.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(5,1,0,'kadu-nina-05.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(6,1,0,'kadu-nina-06.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(7,1,0,'kadu-nina-07.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(8,1,0,'kadu-nina-08.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(9,1,0,'kadu-nina-09.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(10,1,0,'kadu-nina-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(11,1,0,'kadu-nina-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(12,1,0,'kadu-nina-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(13,1,0,'kadu-nina-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(14,2,0,'morar-mais-rj-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(15,2,0,'morar-mais-rj-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(16,2,0,'morar-mais-rj-23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(17,2,0,'morar-mais-rj-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(18,2,0,'morar-mais-rj-28.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(19,2,0,'morar-mais-rj-29.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(20,2,0,'morar-mais-rj-35.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(21,2,0,'morar-mais-rj-37.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(22,2,0,'morar-mais-rj-39.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(23,2,0,'morar-mais-rj-42.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(24,2,0,'morar-mais-rj-45.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(25,3,0,'barra-01.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(26,3,0,'barra-02.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(27,3,0,'barra-03.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(28,3,0,'barra-04.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(29,3,0,'barra-05.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(30,3,0,'barra-06.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(31,3,0,'barra-07.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(32,3,0,'barra-08.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(33,3,0,'barra-09.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(34,3,0,'barra-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(35,3,0,'barra-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(36,3,0,'barra-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(37,3,0,'barra-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(38,4,0,'sede-voran-tecnologia-06.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(39,4,0,'sede-voran-tecnologia-07.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(40,4,0,'sede-voran-tecnologia-08.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(41,4,0,'sede-voran-tecnologia-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(42,4,0,'sede-voran-tecnologia-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(43,4,0,'sede-voran-tecnologia-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(44,4,0,'sede-voran-tecnologia-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(45,4,0,'sede-voran-tecnologia-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(46,4,0,'sede-voran-tecnologia-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(47,4,0,'sede-voran-tecnologia-27.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(48,4,0,'sede-voran-tecnologia-32.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(49,4,0,'sede-voran-tecnologia-36.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(50,4,0,'sede-voran-tecnologia-38.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(51,4,0,'sede-voran-tecnologia-44.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(52,4,0,'sede-voran-tecnologia-46.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(53,4,0,'sede-voran-tecnologia-50.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(54,4,0,'sede-voran-tecnologia-51.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(55,5,0,'ap-central-park-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(56,5,0,'ap-central-park-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(57,5,0,'ap-central-park-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(58,5,0,'ap-central-park-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(59,5,0,'ap-central-park-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(60,5,0,'ap-central-park-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(61,5,0,'ap-central-park-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(62,5,0,'ap-central-park-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(63,5,0,'ap-central-park-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(64,5,0,'ap-central-park-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(65,5,0,'ap-central-park-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(66,5,0,'ap-central-park-2-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(67,5,0,'ap-central-park-2-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(68,5,0,'ap-central-park-2-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(69,5,0,'ap-central-park-2-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(70,5,0,'ap-central-park-2-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(71,5,0,'ap-central-park-2-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(72,5,0,'ap-central-park-2-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(73,5,0,'ap-central-park-2-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(74,5,0,'ap-central-park-2-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(75,5,0,'ap-central-park-2-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(76,5,0,'ap-central-park-2-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(77,5,0,'ap-central-park-2-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(78,5,0,'ap-central-park-2-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(79,5,0,'ap-central-park-2-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(80,5,0,'ap-central-park-2-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(81,5,0,'ap-central-park-2-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(82,5,0,'ap-central-park-2-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(83,5,0,'ap-central-park-2-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(84,5,0,'ap-central-park-2-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(85,5,0,'ap-central-park-2-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(86,5,0,'ap-central-park-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(87,5,0,'ap-central-park-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(88,5,0,'ap-central-park-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(89,5,0,'ap-central-park-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(90,5,0,'ap-central-park-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(91,5,0,'ap-central-park-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(92,5,0,'ap-central-park-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(93,5,0,'ap-central-park-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(94,6,0,'ap-mooca-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(95,6,0,'ap-mooca-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(96,6,0,'ap-mooca-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(97,6,0,'ap-mooca-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(98,6,0,'ap-mooca-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(99,6,0,'ap-mooca-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(100,6,0,'ap-mooca-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(101,6,0,'ap-mooca-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(102,6,0,'ap-mooca-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(103,6,0,'ap-mooca-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(104,6,0,'ap-mooca-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(105,6,0,'ap-mooca-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(106,6,0,'ap-mooca-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(107,6,0,'ap-mooca-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(108,6,0,'ap-mooca-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(109,6,0,'ap-mooca-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(110,6,0,'ap-mooca-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(111,6,0,'ap-mooca-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(112,6,0,'ap-mooca-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(113,6,0,'ap-mooca-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(114,7,0,'ap-central-park-2-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(115,7,0,'ap-central-park-2-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(116,7,0,'ap-central-park-2-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(117,7,0,'ap-central-park-2-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(118,7,0,'ap-central-park-2-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(119,7,0,'ap-central-park-2-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(120,7,0,'ap-central-park-2-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(121,7,0,'ap-central-park-2-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(122,7,0,'ap-central-park-2-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(123,7,0,'ap-central-park-2-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(124,7,0,'ap-central-park-2-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(125,7,0,'ap-central-park-2-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(126,7,0,'ap-central-park-2-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(127,7,0,'ap-central-park-2-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(128,7,0,'ap-central-park-2-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(129,7,0,'ap-central-park-2-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(130,7,0,'ap-central-park-2-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(131,7,0,'ap-central-park-2-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(132,7,0,'ap-central-park-2-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(133,7,0,'ap-central-park-2-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(134,7,0,'ap-central-park-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(135,8,0,'ap-campo-belo-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(136,8,0,'ap-campo-belo-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(137,8,0,'ap-campo-belo-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(138,8,0,'ap-campo-belo-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(139,8,0,'ap-campo-belo-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(140,8,0,'ap-campo-belo-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(141,8,0,'ap-campo-belo-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(142,8,0,'ap-campo-belo-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(143,8,0,'ap-campo-belo-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(144,8,0,'ap-campo-belo-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(145,8,0,'ap-campo-belo-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(146,8,0,'ap-campo-belo-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(147,8,0,'ap-campo-belo-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(148,8,0,'ap-campo-belo-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(149,8,0,'ap-campo-belo-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(150,9,0,'hall-tatuape-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(151,9,0,'hall-tatuape-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(152,9,0,'hall-tatuape-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(153,9,0,'hall-tatuape-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(154,9,0,'hall-tatuape-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(155,9,0,'hall-tatuape-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(156,9,0,'hall-tatuape-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(157,9,0,'hall-tatuape-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(158,9,0,'hall-tatuape-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(159,9,0,'hall-tatuape-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(160,9,0,'hall-tatuape-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(161,9,0,'hall-tatuape-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(162,9,0,'hall-tatuape-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(163,10,0,'brooklin-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(164,10,0,'brooklin-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(165,10,0,'brooklin-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(166,10,0,'brooklin-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(167,10,0,'brooklin-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(168,10,0,'brooklin-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(169,10,0,'brooklin-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(170,10,0,'brooklin-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(171,10,0,'brooklin-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(172,10,0,'brooklin-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(173,10,0,'brooklin-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(174,10,0,'brooklin-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(175,10,0,'brooklin-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(176,10,0,'brooklin-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(177,10,0,'brooklin-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(178,10,0,'brooklin-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(179,10,0,'brooklin-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(180,11,0,'tambore-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(181,11,0,'tambore-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(182,11,0,'tambore-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(183,11,0,'tambore-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(184,11,0,'tambore-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(185,11,0,'tambore-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(186,11,0,'tambore-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(187,11,0,'tambore-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(188,11,0,'tambore-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(189,11,0,'tambore-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(190,11,0,'tambore-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(191,11,0,'tambore-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(192,11,0,'tambore-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(193,11,0,'tambore-21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(194,11,0,'tambore-22.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(195,11,0,'tambore-23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(196,11,0,'tambore-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(197,11,0,'tambore-25.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(198,11,0,'tambore-26.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(199,11,0,'tambore-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(200,11,0,'tambore-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(201,11,0,'tambore-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(202,11,0,'tambore-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(203,11,0,'tambore-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(204,11,0,'tambore-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(205,11,0,'tambore-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(206,12,0,'estudio-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(207,12,0,'estudio-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(208,12,0,'estudio-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(209,12,0,'estudio-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(210,12,0,'estudio-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(211,12,0,'estudio-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(212,12,0,'estudio-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(213,12,0,'estudio-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(214,12,0,'estudio-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(215,13,0,'vila-mariana-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(216,13,0,'vila-mariana-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(217,13,0,'vila-mariana-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(218,13,0,'vila-mariana-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(219,13,0,'vila-mariana-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(220,13,0,'vila-mariana-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(221,13,0,'vila-mariana-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(222,13,0,'vila-mariana-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(223,13,0,'vila-mariana-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(224,13,0,'vila-mariana-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(225,13,0,'vila-mariana-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(226,13,0,'vila-mariana-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(227,13,0,'vila-mariana-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(228,14,0,'bela-vista-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(229,14,0,'bela-vista-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(230,14,0,'bela-vista-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(231,14,0,'bela-vista-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(232,14,0,'bela-vista-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(233,14,0,'bela-vista-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(234,14,0,'bela-vista-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(235,14,0,'bela-vista-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(236,14,0,'bela-vista-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(237,15,0,'alphaville.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(238,15,0,'alphaville10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(239,15,0,'alphaville11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(240,15,0,'alphaville12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(241,15,0,'alphaville13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(242,15,0,'alphaville14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(243,15,0,'alphaville15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(244,15,0,'alphaville16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(245,15,0,'alphaville17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(246,15,0,'alphaville18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(247,15,0,'alphaville19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(248,15,0,'alphaville2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(249,15,0,'alphaville3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(250,15,0,'alphaville4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(251,15,0,'alphaville5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(252,15,0,'alphaville6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(253,15,0,'alphaville7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(254,15,0,'alphaville8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(255,15,0,'alphaville9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(256,16,0,'centro1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(257,16,0,'centro10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(258,16,0,'centro11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(259,16,0,'centro12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(260,16,0,'centro13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(261,16,0,'centro14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(262,16,0,'centro15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(263,16,0,'centro16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(264,16,0,'centro17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(265,16,0,'centro18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(266,16,0,'centro19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(267,16,0,'centro2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(268,16,0,'centro20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(269,16,0,'centro21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(270,16,0,'centro22.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(271,16,0,'centro23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(272,16,0,'centro24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(273,16,0,'centro3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(274,16,0,'centro4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(275,16,0,'centro5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(276,16,0,'centro6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(277,16,0,'centro7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(278,16,0,'centro8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(279,16,0,'centro9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(280,17,0,'leopoldina1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(281,17,0,'leopoldina10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(282,17,0,'leopoldina11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(283,17,0,'leopoldina12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(284,17,0,'leopoldina13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(285,17,0,'leopoldina14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(286,17,0,'leopoldina2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(287,17,0,'leopoldina3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(288,17,0,'leopoldina4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(289,17,0,'leopoldina5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(290,17,0,'leopoldina6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(291,17,0,'leopoldina7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(292,17,0,'leopoldina8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(293,17,0,'leopoldina9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(294,18,0,'panamby-01.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(295,18,0,'panamby-02.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(296,18,0,'panamby-03.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(297,18,0,'panamby-04.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(298,18,0,'panamby-05.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(299,18,0,'panamby-06.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(300,18,0,'panamby-07.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(301,18,0,'panamby-08.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(302,18,0,'panamby-09.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(303,18,0,'panamby-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(304,18,0,'panamby-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(305,18,0,'panamby-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(306,18,0,'panamby-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(307,18,0,'panamby-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(308,18,0,'panamby-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(309,18,0,'panamby-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(310,18,0,'panamby-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(311,18,0,'panamby-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(312,18,0,'panamby-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(313,18,0,'panamby-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(314,18,0,'panamby-21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(315,18,0,'panamby-22.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(316,18,0,'panamby-23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(317,18,0,'panamby-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(318,18,0,'panamby-25.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(319,19,0,'esmalteca-01.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(320,19,0,'esmalteca-02.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(321,19,0,'esmalteca-03.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(322,19,0,'esmalteca-04.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(323,19,0,'esmalteca-05.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(324,19,0,'esmalteca-06.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(325,19,0,'esmalteca-07.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(326,19,0,'esmalteca-08.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(327,19,0,'esmalteca-09.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(328,19,0,'esmalteca-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(329,19,0,'esmalteca-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(330,19,0,'esmalteca-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(331,19,0,'esmalteca-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(332,19,0,'esmalteca-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(333,19,0,'esmalteca-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(334,19,0,'esmalteca-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(335,19,0,'esmalteca-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(336,19,0,'esmalteca-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(337,19,0,'esmalteca-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(338,19,0,'esmalteca-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(339,19,0,'esmalteca-21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(340,20,0,'casa-brooklin-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(341,20,0,'casa-brooklin-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(342,20,0,'casa-brooklin-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(343,20,0,'casa-brooklin-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(344,20,0,'casa-brooklin-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(345,20,0,'casa-brooklin-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(346,20,0,'casa-brooklin-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(347,20,0,'casa-brooklin-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(348,20,0,'casa-brooklin-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(349,20,0,'casa-brooklin-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(350,20,0,'casa-brooklin-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(351,20,0,'casa-brooklin-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(352,20,0,'casa-brooklin-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(353,20,0,'casa-brooklin-21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(354,20,0,'casa-brooklin-22.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(355,20,0,'casa-brooklin-23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(356,20,0,'casa-brooklin-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(357,20,0,'casa-brooklin-25.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(358,20,0,'casa-brooklin-26.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(359,20,0,'casa-brooklin-27.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(360,20,0,'casa-brooklin-28.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(361,20,0,'casa-brooklin-29.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(362,20,0,'casa-brooklin-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(363,20,0,'casa-brooklin-30.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(364,20,0,'casa-brooklin-31.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(365,20,0,'casa-brooklin-32.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(366,20,0,'casa-brooklin-33.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(367,20,0,'casa-brooklin-34.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(368,20,0,'casa-brooklin-35.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(369,20,0,'casa-brooklin-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(370,20,0,'casa-brooklin-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(371,20,0,'casa-brooklin-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(372,20,0,'casa-brooklin-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(373,20,0,'casa-brooklin-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(374,20,0,'casa-brooklin-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(375,21,0,'apto-morumbi-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(376,21,0,'apto-morumbi-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(377,21,0,'apto-morumbi-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(378,21,0,'apto-morumbi-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(379,21,0,'apto-morumbi-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(380,21,0,'apto-morumbi-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(381,21,0,'apto-morumbi-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(382,21,0,'apto-morumbi-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(383,21,0,'apto-morumbi-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(384,21,0,'apto-morumbi-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(385,21,0,'apto-morumbi-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(386,21,0,'apto-morumbi-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(387,21,0,'apto-morumbi-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(388,21,0,'apto-morumbi-21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(389,21,0,'apto-morumbi-22.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(390,21,0,'apto-morumbi-23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(391,21,0,'apto-morumbi-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(392,21,0,'apto-morumbi-25.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(393,21,0,'apto-morumbi-26.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(394,21,0,'apto-morumbi-27.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(395,21,0,'apto-morumbi-28.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(396,21,0,'apto-morumbi-29.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(397,21,0,'apto-morumbi-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(398,21,0,'apto-morumbi-30.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(399,21,0,'apto-morumbi-31.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(400,21,0,'apto-morumbi-32.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(401,21,0,'apto-morumbi-33.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(402,21,0,'apto-morumbi-34.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(403,21,0,'apto-morumbi-35.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(404,21,0,'apto-morumbi-36.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(405,21,0,'apto-morumbi-37.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(406,21,0,'apto-morumbi-38.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(407,21,0,'apto-morumbi-39.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(408,21,0,'apto-morumbi-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(409,21,0,'apto-morumbi-40.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(410,21,0,'apto-morumbi-41.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(411,21,0,'apto-morumbi-42.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(412,21,0,'apto-morumbi-43.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(413,21,0,'apto-morumbi-44.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(414,21,0,'apto-morumbi-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(415,21,0,'apto-morumbi-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(416,21,0,'apto-morumbi-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(417,21,0,'apto-morumbi-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(418,21,0,'apto-morumbi-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(419,22,0,'apto-moema-1.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(420,22,0,'apto-moema-10.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(421,22,0,'apto-moema-11.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(422,22,0,'apto-moema-12.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(423,22,0,'apto-moema-13.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(424,22,0,'apto-moema-14.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(425,22,0,'apto-moema-15.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(426,22,0,'apto-moema-16.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(427,22,0,'apto-moema-17.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(428,22,0,'apto-moema-18.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(429,22,0,'apto-moema-19.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(430,22,0,'apto-moema-2.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(431,22,0,'apto-moema-20.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(432,22,0,'apto-moema-21.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(433,22,0,'apto-moema-22.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(434,22,0,'apto-moema-23.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(435,22,0,'apto-moema-24.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(436,22,0,'apto-moema-25.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(437,22,0,'apto-moema-26.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(438,22,0,'apto-moema-27.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(439,22,0,'apto-moema-28.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(440,22,0,'apto-moema-29.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(441,22,0,'apto-moema-3.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(442,22,0,'apto-moema-30.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(443,22,0,'apto-moema-31.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(444,22,0,'apto-moema-32.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(445,22,0,'apto-moema-33.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(446,22,0,'apto-moema-34.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(447,22,0,'apto-moema-4.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(448,22,0,'apto-moema-5.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(449,22,0,'apto-moema-6.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(450,22,0,'apto-moema-7.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(451,22,0,'apto-moema-8.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41'),(452,22,0,'apto-moema-9.jpg','2016-04-06 19:25:41','2016-04-06 19:25:41');
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$l0nG4tDMkZ0OLv2JLe/Qn.0Dbi9JmtU.fD4p5qAhYlXghW1KSJsBe',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-06 19:28:51
